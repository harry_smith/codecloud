'use strict';

angular.module('codecloudFullstackApp')
  .controller('MainCtrl', function ($scope, $http, Auth, AppState, $location, ngDialog) {
    // bind with service to share global app state
   

    // bind with service to share global app state.
    //$scope.localScope = AppState;
    
    $scope.sharedScope = AppState.data;

    $scope.query = $scope.sharedScope.query;

    
    // setup user snippet
    $scope.snippetList = {};
    // get current user 
    $scope.getCurrentUser = Auth.getCurrentUser;
    // get current user ID
    $scope.CurrentUserID = $scope.getCurrentUser()._id;
    //console.log("userID: " + $scope.CurrentUserID);
    // determine is a user is logged in
    $scope.isLoggedIn = Auth.isLoggedIn;
    // if user is logged in - retrieve user snippets
    if ($scope.isLoggedIn){
      // if user logged in then request
      $http.get('/api/snippets/user/'+$scope.CurrentUserID).success(function(userSnippets) {
        //console.log($scope.userSnippets);
      }).success(function(snippetResponse) {
        //console.log(snippetResponse);
        $scope.loadBrowser(snippetResponse);
        // handle errors
      }).error(function(data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
    }
    // Live Search Filter that analyses title, decription and all tags per snippet - returns true if any are found
    $scope.search = function (snippet){
      // if we aren't in search mode i.e the search field is empty - return true & show all snippets
      if (!$scope.sharedScope.searchMode){
        return true;
      }
      // stringify tag array and check for query string
      function search_for_string_in_array(str, arr) {
          var json = JSON.stringify(arr);
          //check for query string
          if (json.toLowerCase().indexOf($scope.sharedScope.query )!=-1) {
            // show this nippet in search results
            return true;
          }
      }
      // determine a match for topic tags using query string 
      var topicTagsMatch = search_for_string_in_array($scope.sharedScope.query, snippet.topicTags);
      // determine a match for language tags using query string 
      var languageTagsMatch = search_for_string_in_array($scope.sharedScope.query, snippet.languageTags);
      // determine a match for project tags using query string 
      var projectTagsMatch = search_for_string_in_array($scope.sharedScope.query, snippet.projectTags);
      // if any of these tags match query string then show this snippet in search results
      if (topicTagsMatch || languageTagsMatch || projectTagsMatch){
        return true;
      }
      // also checkout title and description for matches with query string
      if (snippet.title.toLowerCase().indexOf($scope.sharedScope.query)!=-1 ||
        snippet.description.toLowerCase().indexOf($scope.sharedScope.query)!=-1)
      {
        // there is a match so show in search results
        return true;
      }
      // no match has been found so ignore this snippet in search results
      return false;
    };
    $scope.loadBrowser = function( snippets ) {
      //assign returned snippets to the local scope
      $scope.snippetList = snippets;
    };
    // copy contents of snippet to clipboard
    $scope.copyToClipboard = function(snippet) {
      var code = snippet.snippet;
      window.prompt("Copy to clipboard: Ctrl+C / ⌘+C Enter", code);
    };

    $scope.editSnippet = function(snippet) {
      // move to edit mode
      $location.path( "/editor/" + snippet._id);
    };

    $scope.previewSnippet = function(snippet) {
      ngDialog.open({
        template:'codemirrorTemplate', 
        controller: ['$scope','ngDialog', function($scope) {
          // controller logic
          // CodeMirror Setup 
          $scope.editorOptions = {
              lineWrapping : true,
              lineNumbers: true,
              mode: 'javascript',
              theme: 'monokai'
          };
          // code editor content
          $scope.editorContent = snippet.snippet;
        }]
      
      });
    };

    $scope.deleteSnippet = function(event, snippet) {
      // delete snippet
      $http.delete('/api/snippets/' + snippet._id)
        .success(function(data, status, headers, config) {
          // animate out on successful delete
         // $(event.target).parent().parent().parent().addClass('zoomOutDown');
          //$(event.target).parent().parent().parent().addClass('animated');

          //implement this after timeout
          var index = $scope.snippetList.indexOf(snippet)
          $scope.snippetList.splice(index, 1); 
        });
    };
  });
