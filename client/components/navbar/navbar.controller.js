'use strict';
angular.module('codecloudFullstackApp')
  .controller('NavbarCtrl', function ($scope, $location, Auth, AppState, $state) {
    // bind with service to share global app state.
    $scope.sharedScope = AppState.data;
    // watch for change to the query field
    $scope.$watch("query", function() { 
      if ($scope.query){
        // update the shared query parameter
        $scope.sharedScope.query = $scope.query.toLowerCase();
      }
    }, true);

    $scope.menu = [{
      'title': 'Home',
      'link': '/'
    }];
    // get the logged in user auth
    $scope.isLoggedIn = Auth.isLoggedIn;
    // determine if they are admin
    $scope.isAdmin = Auth.isAdmin;
    // get logged in user
    $scope.getCurrentUser = Auth.getCurrentUser;

    $scope.clear = function(){
      // if search input is empty exit search mode
      if($scope.query.length == 0){
        //delete $scope.sharedScope.query;
        $scope.sharedScope.searchMode = false;
      } else {
        // if search input isn't empty enter search mode
        $scope.sharedScope.searchMode = true;
      }
    }
    $scope.setEditorMode = function() {
      // if editor page
      if ($state.current.name == "editor.new" || $state.current.name == "editor.view"){
        // set to true
        $scope.sharedScope.state = true;
      } else {
        // other wise set false
        $scope.sharedScope.state = false;
      }
    }
    // set global state based on location url
    $scope.init = function () {
      // update state
      $scope.setEditorMode();
    }
    // run init to set state on load
    $scope.init();
    $scope.logout = function() {
      Auth.logout();
      $location.path('/login');
    };
    // on each page change
    $scope.$on('$locationChangeStart', function(event) {
      // update state
      $scope.setEditorMode();
    });
    $scope.isActive = function(viewLocation) {
      return viewLocation === $location.path();
    };
  });